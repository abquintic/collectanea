# README.md

## Preliminary E-Book Library Organizer
This is a categorization and organization tool organizing your gaming PDF collection.

Features will include:
* File system organization by Publisher, Game Line, and Genre.
* Numerous tags for additional categorization
* User built tags
* Deep integration with the RPG Card Catalog
* Plugin interface for importing from other metadata sources
* Launch preferred reading tool.
* Plugin interface for other file formats