var application_menu = [
    {
      label: 'Application',
      submenu: [
        {
          id: 'configuration',
          label: 'Configuration',
          accelerator: 'CmdOrCtrl+O',
          click: () => {
            openConfiguration();
          }
        },
        {
          id: 'manage_plugins',
          label: 'Manage Plugins',
          accelerator: 'CmdOrCtrl+P',
          click: () => {
          }
        },
        {
          id: 'debug',
          label: 'Debug',
          accelerator: 'CmdOrCtrl+D',
          click: () => {
            mainWindow.webContents.toggleDevTools();
          }
        }
      ]
    },
    {
      label: 'Database',
      submenu: [
        {
          id: 'import',
          label: 'Import',
          submenu: [
            {
              id: 'import_file',
              label: 'Import File',
              accelerator: 'CmdOrCtrl+F',
              click: () => {
                dialog.showOpenDialog({ filters: [ { name: 'E-Books', extensions: ['pdf'] } ], properties: [ 'openFile' ] }).then((data) => {
                  if ( ! data.canceled ) {
                    parsePDF(data.filePaths);
                  }
                });
              }
            },
            {
              id: 'import_directory',
              label: 'Import Directory',
              accelerator: 'CmdOrCtrl+D',
              click: () => {
                dialog.showOpenDialog({ properties: [ 'openDirectory' ]}).then((data) => {
                  if ( ! data.canceled ) {
                    parseDirectory(data.filePaths);
                  }
                });
              }
            }
          ]
        },
        {
          id:'export_database',
          label: 'Export Database',
          click: () => {
            dialog.showOpenDialog({ properties: [ 'openFile', 'openDirectory', 'multiSelections' ]});
          }
        }

      ]
    }
  ];

  if (process.platform == 'darwin') {
    const name = app.getName();
    application_menu.unshift({
      label: name,
      submenu: [
        {
          label: 'About ' + name,
          role: 'about'
        },
        {
          type: 'separator'
        },
        {
          label: 'Services',
          role: 'services',
          submenu: []
        },
        {
          type: 'separator'
        },
        {
          label: 'Hide ' + name,
          accelerator: 'Command+H',
          role: 'hide'
        },
        {
          label: 'Hide Others',
          accelerator: 'Command+Shift+H',
          role: 'hideothers'
        },
        {
          label: 'Show All',
          role: 'unhide'
        },
        {
          type: 'separator'
        },
        {
          label: 'Quit',
          accelerator: 'Command+Q',
          click: () => { app.quit(); }
        },
      ]
    });
  }

module.exports = {application_menu}