const fs = require('fs');
const ipc = require("electron").ipcRenderer;
const crypto = require('crypto')
const path = require('path');
const waterfall = require('async-waterfall');
const Tabulator = require('tabulator-tables');

var MetaDataFromDatabase = {};  
let MetaDataFromDatabaseUpdated = {};
let installedPlugins = [];

MetaDataFromDatabaseUpdated.PDF = {};

// Row Context menus
const pdfTableRowMenu = [
  {
    label: 'View PDF ',
    action: function contextMenuLaunchEdit( e, row ){
     rowData = row.getData();
     whichTitle = rowData.index;
     ipc.send("launchTitle", whichTitle );           
    }
  },
  {
     label: 'Edit PDF Data',
     action: function contextMenuLaunchEdit( e, row ){
       rowData = row.getData();
       whichTitle = rowData.index;
       ipc.send("editTitle", whichTitle );           
     }
   },
   {
    label: 'Delete PDF',
    action: function contextMenuLaunchEdit( e, row ){
      rowData = row.getData();
      whichTitle = rowData.index;
      ipc.send("deleteTitle", whichTitle );           
    }
  },
 ]


var tabulatorObject = null;
var metadataTabulatorObject = null;
var gridData = null;
var lastWindowSizeHeight = 0;
var lastWindowSizeWidth = 0;
var tableRowHeight = -1;

var editMetadataBlock, multiInsertBlock, configBlock, metadataPluginBlock;

let PluginsReporting = [];
let pluginResponsesComing = 0;
let pluginResponsesIn = 0;
let totalPluginsExpected = 0;

// PDF.js
var PDFJS= require("pdfjs-dist");
const { config } = require('process');
const { autoUpdater } = require('electron');
PDFJS.GlobalWorkerOptions.workerSrc = '../node_modules/pdfjs-dist/build/pdf.worker.min.js'

function Deferred() {
  var self = this;
  this.promise = new Promise(function(resolve, reject) {
    self.reject = reject
    self.resolve = resolve
  })
}

const modeMain = Symbol("mainWindow");
const modeMultiInsert = Symbol("multiInsert");
const modeEditMetaData = Symbol("editMetaData");
const modeEditConfig = Symbol("config");
const modeSelectMetaData = Symbol("selectMetaData");

var pageMode = modeMain;
var setFormEditted = false;

let headerButtons = [ "btn_main_config", "btn_main_plugins", "btn_main_import", "btn_main_massimport", "btn_main_export", 
"btn_main_collection", "btn_main_search", "btn_main_close", "btn_main_maximize", "btn_main_minimize", "btn_main_tray",
'btn_massImportPDF_save', 'btn_massImportPDF_reset', 'btn_massImportPDF_cancel', 'btn_massImportPDF_skip',
'btn_main_editPDF_save', 'btn_main_editPDF_reset', 'btn_main_editPDF_skip', 'btn_main_editPDF_cancel', 'btn_main_editPDF_reloadMetaFromFile', ];

let formFields = [ "productline.gameline", "productline.required", "production.publisher", "production.sequentialtitle", 
"production.title", "production.pagecount", "production.pageformat", "production.fileformat", "playstyle.gametype", 
"playstyle.genres", "playstyle.subgenres", "playstyle.gm", "playstyle.playeragency", "playstyle.crunchiness", 
"production.rights", "production.url", "published" ]

let metaDataPluginButtons = [ "btn_metaData_cancel" ];

function editPDFSetEditted() {
  if ( setFormEditted ) { return; }
  buttonList = [];
  if (pageMode == modeEditMetaData) {
    buttonList.push('btn_main_editPDF_reset');
    buttonList.push('btn_main_editPDF_save');
    buttonList.push('btn_main_editPDF_skip');
    buttonList.push('btn_main_editPDF_cancel');
  }
  for ( button in buttonList ) {
    document.getElementById(buttonList[ button ]).className = document.getElementById(buttonList[ button ]).className.replace("btn-default", "btn-warning");
  }
  setFormEditted = true;
}

function editPDFAddBindings() {
  for (field in formFields ) {
    document.getElementById(formFields[field]).addEventListener('input', function fieldOnInput( event ) { editPDFSetEditted(); });
  }
  buttonList = ['btn_main_editPDF_reset','btn_main_editPDF_save', 'btn_main_editPDF_skip', 'btn_main_editPDF_cancel', 'btn_main_editPDF_reloadMetaFromFile'];
  for ( button in buttonList ) {
    document.getElementById(buttonList[ button ]).addEventListener("click", function editPDFFormButtonClicked(e) {
      let src = e.target;
      if( src.children.length === 0 ) { src = src.parentElement; }
      console.info(src.id + " clicked.");
      ipc.send(src.id,MetaDataFromDatabase.PDF);
     } );
  }
}

// based on https://codepen.io/juliendargelos/pen/MJjJZm - unsure I'm completely happy with it.

var TagsCloudInput = function(element, edittable) {
  var self = this;
  var initChar = "\u200B";
  var initCharPattern = new RegExp(initChar, 'g');

  var insert = function(element) {
     if(self.textNode) { self.element.parentElement.insertBefore(element, self.element); }
     else self.element.appendChild(element);
  };

  var updateCursor = function() {
    self.cursor = self.blank;
  };

  var keydown = function(event) {
    if((event.keyCode == 188) || (event.keyCode == 13)) {
      event.preventDefault();
      setTimeout(function() {
        var text = self.text;
        if(text) {
          self.text = initChar;
          self.add(text);
        }
      }, 1);
    }
    else if(event.keyCode == 8) {
      if(self.text.replace(initCharPattern, '') == '') {
        self.text = initChar+initChar;
        if(self.selected) {
          self.element.removeChild(self.selected);
        }
        else {
          var tags = self.tags;
          var keys = Object.keys(tags)
          if(keys.length > 0) {
            var tag = tags[keys[keys.length-1]];
            tag.setAttribute('data-selected', '');
          }
        }
      }
    }

    if(event.keyCode !== 8) {
      if(self.selected) self.selected.removeAttribute('data-selected');
    }
    setTimeout(function() {
      updateCursor();
    }, 1);
  };

  var focus = function() {
    updateCursor();
  };

  Object.defineProperties(this, {
    element: {
      get: function() {
        return element;
      },
      set: function(v) {
        if(typeof v == 'string') v = document.getElementById(v);
        element = v instanceof Node ? v : document.createElement('div');
        if(!element.className.match(/\btags-input\b/)) element.className += ' tags-input';
        if ( edittable ) {
          if(element.getAttribute('contenteditable') != 'true') element.setAttribute('contenteditable', 'true');
        }

        element.removeEventListener('keydown', keydown);
        element.addEventListener('keydown', keydown);

        element.removeEventListener('focus', focus);
        element.addEventListener('focus', focus);
        this.text = initChar;
      }
    },
    tags: {
      get: function() {
        var element;
        var elements = this.element.parentElement.querySelectorAll('button');
        var tagsArray = [];
        for(var i = 0; i < elements.length; i++) {
          element = elements[i]
          tagsArray.push( element.innerText );
        }

        return tagsArray;
      }
    },
    lastChild: {
      get: function() {
        return this.element.lastChild;
      }
    },
    textNode: {
      get: function() {
        return this.element.lastChild instanceof Text ? this.element.lastChild : null;
      }
    },
    text: {
      get: function() {
        return this.textNode ? this.textNode.data : null;
      },
      set: function(v) {
        if(!this.textNode) this.element.appendChild(document.createTextNode(','));
        this.textNode.data = v;
      },
    },
    cursor: {
      get: function() {
        return this.element.getAttribute('data-cursor') !== null;
      },
      set: function(v) {
        if(v) this.element.setAttribute('data-cursor', '');
        else this.element.removeAttribute('data-cursor');
      }
    },
    focused: {
      get: function() {
        return document.activeElement == this.element;
      }
    },
    blank: {
      get: function() {
        if ( !this.text ) { return false; }
        return this.text.replace(initCharPattern, '') == '';
      }
    },
    selected: {
      get: function() {
        return this.element.querySelector('span[data-selected]');
      }
    }
  });

  this.add = function(tag) {
    tag = tag.replace(initCharPattern, '');
    tag = tag.replace(/^\s+/, '').replace(/\s+$/, '');
    if(tag != '' && this.tags[tag] === undefined) {
      var element = document.createElement('button');
      element.appendChild(document.createTextNode(tag));
      element.setAttribute('contenteditable', 'false');
      var removeSpan = document.createElement('span');
      removeSpan.className = "icon icon-cancel";
      removeSpan.style.paddingLeft = "4px";
      removeSpan.addEventListener("click", function(evt) { evt.target.parentElement.parentElement.removeChild( evt.target.parentElement ); } );
      element.appendChild(removeSpan);

      insert(element);
    }
  };

  this.remove = function(tag) {
     var element = this.tags[tag];
     if(element) this.element.removeChild(element);
     else {
       let buttons = this.element.parentElement.getElementsByTagName('button');
       for( var button=0; button < buttons.length; button++ )
       {
         if ( buttons[ button ].innerText == tag )
         {
           this.element.parentElement.removeChild(buttons[ button ]);
         }
       }
     }
  };

  this.clear = function() {
    let originalTags = this.tags;
    for ( var l=0; l < originalTags.length; l++) { this.remove( originalTags[l]);}
  }

  this.element = element;
};

function splitAppend( appendArray, sourceString ) {
  let returnArray;
  if ( appendArray && appendArray != undefined ) { returnArray = appendArray.slice(0); } else { returnArray = []; }
  if( sourceString && sourceString != undefined )
  {
    // Replace & and "and" with commas.
    newString = sourceString.replace( ' & ', ', ' ).replace(/ and /i, ', ' );
    let sploidy = newString.split(',');
    if( sploidy.length == 1) { returnArray.push( sploidy[ 0 ] ); }
    else {
      for ( var loop = 0; loop < sploidy.length; loop = loop + 1) {
        returnArray.push( sploidy[ loop ].trim() );
      }
    }
    return( returnArray );
  }
  return null;
}

contextMenuClickedID = null;
let TagClouds = [];

function setClassByID( htmlID, className ) {
  if ( document.getElementById( htmlID ) ) {
    document.getElementById( htmlID ).className = className;
  } 
  // else {
  //   console.info("Did not find element " + htmlID);
  // }
}

function populateMetadataPluginButtons (){
  // Might not always be the only one.
  document.getElementById('btn_metaData_cancel').addEventListener("click",cancelMetadataSearch);
}

function setMode() {

  // Clear Activity Divs
  setClassByID("mainBody", "hiddenMainBody");
  let multi = document.getElementById('massImportArea');
  if (multi) multi.remove();
  let edit = document.getElementById('editArea');
  if (edit) edit.remove();
  let config = document.getElementById('configureImportArea');
  if (config) config.remove();
  let metaplugin = document.getElementById('metaDataDisplayArea');
  if (metaplugin) metaplugin.remove();

  let body = document.getElementsByTagName('body')[0];

  if ( pageMode == modeMain ) {
    setClassByID("mainBody", "mainBody");
  } else if ( pageMode == modeMultiInsert ) {
    let newElem = multiInsertBlock.cloneNode(true);
    body.appendChild(newElem);
    setClassByID("massImportArea", "massImportPDFBody");
  } else if ( pageMode == modeEditMetaData ) {
    let newElem = editMetadataBlock.cloneNode(true);
    body.appendChild(newElem);
    editPDFAddBindings();
    setClassByID("editArea", "editpdfBody");
    setFormEditted = false;
    populateMetadataPlugins();
  } else if ( pageMode == modeEditConfig ) {
    let newElem = configBlock.cloneNode(true);
    body.appendChild(newElem);
    setClassByID("configureArea", "editConfigurationBody");
  } else if ( pageMode == modeSelectMetaData ) {
    let newElem = metadataPluginBlock.cloneNode(true);
    body.appendChild(newElem);
    populateMetadataPluginButtons();
  }
}

function notEmpty( value, columnName ) {
    if ( ( value ) && ( value != "undefined" ) )
    {
      return( value )
    } else {
      if ( columnName == "Title" ) {
        return( '<i>No Title Found</i>');
      } else {
        return("");
      }
    }
}

function goodProperty( parent, child )
{
  return ( parent.hasOwnProperty(child) && parent[child] );
}

function chooseMetaData( whichRow ) {
  let resultData = JSON.parse(whichRow.target.parentElement.children[0].innerHTML);
  var img = new Image();
  img.crossOrigin = 'Anonymous';
  img.onload = function() {
    var canvas = document.createElement('CANVAS');
    var ctx = canvas.getContext('2d');
    var dataURL;
    canvas.height = this.naturalHeight;
    canvas.width = this.naturalWidth;
    ctx.drawImage(this, 0, 0);
    resultData.production.Thumbnails = canvas.toDataURL('image/jpeg');
    
    ipc.send("metadataSelected", resultData );
  };
  img.src = document.getElementById('metaDataSampleCover').src;
  if (img.complete || img.complete === undefined) {
    img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
    img.src = src;
  }
}

ipc.on('resetForm', function (event, arg) {
  clearForm();
  setForm(MetaDataFromDatabase.PDF);
  if ( !MetaDataFromDatabaseUpdated.updated ) {
    clearButtons();
  } else {
    editPDFSetEditted();
  }
});

ipc.on("metadataresponse", function (event,arg) {
  let dataNewRow = {
    id: JSON.stringify(arg.results.production.Title), 
    index: arg.results.coverURL, 
    title: arg.results.production.Title, 
    publisher: arg.results.production.Publisher,
    authors: arg.results.production.Authors,
    tags: arg.results.tags,
    source: arg.results.source
  }

  if ( (PluginsReporting.length === 0 )  || (PluginsReporting.indexOf(arg.source) < 0 ))
  {
    PluginsReporting.push(arg.source);
    pluginResponsesComing = pluginResponsesComing + arg.resultCount;

    if ( ! metadataTabulatorObject ) {

      let box = document.querySelector('#mainToolbar');
      let style = getComputedStyle(box);
      let marginTop = parseInt(style.marginTop);
      let marginBottom = parseInt(style.marginBottom);
      let borderTopWidth = parseInt(style.borderTopWidth) || 0;
      let borderBottomWidth = parseInt(style.borderBottomWidth) || 0;
      let footerHeight = document.getElementById("metaDataFooter").offsetHeight + marginTop + marginBottom + borderTopWidth + borderBottomWidth;
    
      metadataTabulatorObject = new Tabulator("#metaDataTable", {
        layout:"fitColumns", //fit columns to width of table (optional)
        pagination:true,
        height: document.documentElement.clientHeight - footerHeight - 1 + "px",
        data: [dataNewRow],
        columns:[ //Define Table Columns
          {title:"Index", field:"index", resizable: false, visible:false},
          {title:"Contents", field:"contents", resizeable: false, visible:false},
          {title:"Title", field:"title", resizable:true, formatter:"html"},
          {title:"Publisher", field:"publisher",resizable:true},
          {title:"Authors", field:"authors",resizable:true},
          {title:"Tags", field:"tags",resizable:true},
          {title:"Source", field:"source",resizable:true},
        ],
      });
      metadataTabulatorObject.on("rowClick", function setMetadataCover(e, row) { 
        document.getElementById('metaDataSampleCover').src = e.srcElement.parentElement.children[0].innerHTML; 
      });      
      metadataTabulatorObject.on("rowDblClick", function chooseMetaData(e, row) { 
        if (row) {
          document.getElementById('metaDataSampleCover').src = e.srcElement.parentElement.children[0].innerHTML; 
          chooseMetaData(row);
        }
      });
    }
    pluginResponsesIn = pluginResponsesIn + 1;
  } else {
    metadataTabulatorObject.addData([dataNewRow], false);
  }

  if ( ( PluginsReporting.length == totalPluginsExpected ) && ( pluginResponsesIn == pluginResponsesComing ) )
  {
    // All Done!
  }
});

ipc.on("resetMetaDataResults", function (event, arg) {
  PluginsReporting = [];
  pluginResponsesComing = 0;
  pluginResponsesIn = 0;
  totalPluginsExpected = 0;
});

ipc.on("PluginCount", function( event, arg ) {
  totalPluginsExpected = arg;
});

function toggleMetadataPlugins( clear ) {
  let labels = document.getElementById("metadatacheckboxdiv").getElementsByTagName("label");
  let toggleInput = document.getElementById("CheckThemAllToggle");
  let flipValue = toggleInput.checked;
  if ( clear ) { toggleInput.checked = false; }
  for( var childLoop=1; childLoop < labels.length; childLoop++)
    {
      var setTo = labels[ childLoop ].getElementsByTagName("input")[0];
      setTo.checked = flipValue;
    }
}

function metadatasearch() {
  let selectedPlugins = [];
  let labels = document.getElementById("metadatacheckboxdiv").getElementsByTagName("label");
  for( var childLoop=0; childLoop < labels.length; childLoop++)
    {
      var setTo = labels[ childLoop ].getElementsByTagName("input")[0];
      if( setTo.checked ) {
        selectedPlugins.push(setTo.id);
      }
    }
  if( selectedPlugins.length > 0 ) { 
    ipc.send('metadatasearch', { originalname: MetaDataFromDatabase.PDF.production.Title, currentname: document.getElementById('production.title').value, plugins: selectedPlugins } ); 
    collectEdittedMetaData(jsonPDF());
    pageMode = modeSelectMetaData;
    setMode();
  }
}

function cancelMetadataSearch() {
  pageMode = modeEditMetaData;
  setMode();
  // Restore the snapshot of the edit in progress.
  clearForm();
  setForm( MetaDataFromDatabaseUpdated.PDF, MetaDataFromDatabase.position );
  clearButtons();
}

function populateMetadataPlugins() {
  for ( plugin in installedPlugins ) {
    let arg = installedPlugins[ plugin ];
    let divparent = document.getElementById("metadatacheckboxdiv");
    let label = document.createElement("label");
    let newPluginCheckbox = document.createElement("input");
    let newPluginText = document.createElement("text");

    newPluginCheckbox.type = "checkbox";
    newPluginCheckbox.id = arg.id;
    newPluginText.innerText = arg.shortname;

    label.className = "metaDataCheckboxLabel";

    divparent.appendChild(label);
    label.appendChild(newPluginCheckbox);
    label.appendChild(newPluginText);
  }
  document.getElementById('lookItUpButton').addEventListener("click",metadatasearch);
  document.getElementById('CheckThemAllToggle').addEventListener("click",function(e) {
    toggleMetadataPlugins(false);
  });
}

function formatGridData( titles, reformat ) {
  if (! reformat ) {
    newGridData = []
    if (titles != null) {
      for ( var loop = 0; loop < titles.length; loop++ )
      {
        let rawTitle = notEmpty( titles[loop].production.Title, "Title" );
        newGridRow = {
            id: titles[loop]["$loki"],
            index:titles[loop]["$loki"],
            title: rawTitle,
            authors: notEmpty( titles[loop].production.Authors.join(', '), "Authors"),
            publisher: notEmpty( titles[loop].production.Publisher, "Publisher" ),
            tags: notEmpty( titles[loop].tags.join(', '), "Tags" )
        }
        newGridData.push(newGridRow);
      }
    } 
    gridData = newGridData;
  }

  let box = document.querySelector('#mainToolbar');
  let style = getComputedStyle(box);
  let marginTop = parseInt(style.marginTop);
  let marginBottom = parseInt(style.marginBottom);
  let borderTopWidth = parseInt(style.borderTopWidth) || 0;
  let borderBottomWidth = parseInt(style.borderBottomWidth) || 0;

  let toolbarHeight = document.getElementById("mainToolbar").offsetHeight + marginTop + marginBottom + borderTopWidth + borderBottomWidth;

  tabulatorObject = new Tabulator("#mainDisplayArea", {
    layout:"fitColumns", //fit columns to width of table (optional)
    pagination:true,
    data: gridData,
    rowContextMenu: pdfTableRowMenu,
    height: document.documentElement.clientHeight - toolbarHeight + 1 + "px",
    columns:[ //Define Table Columns
      {title:"Index", field:"index", resizable: false, visible:false},
      {title:"Title", field:"title", resizable:true, formatter:"html"},
      {title:"Authors", field:"authors",resizable:true},
      {title:"Publisher", field:"publisher",resizable:true},
      {title:"Tags", field:"tags",resizable:true},
    ],
  });  

  lastWindowSizeHeight = document.documentElement.clientHeight;
  lastWindowSizeWidth = document.documentElement.clientWidth;
  
  tabulatorObject.on("cellDblClick", function(e, cell){
    ipc.send("launchTitle", e.srcElement.parentElement.children[0].innerHTML);    
  });
}

ipc.on('loadedPlugin', function(event, arg) {
  installedPlugins.push( arg );
});

ipc.on('editPDFData', function (event, arg) {
  // Set backup value
  pageMode = modeEditMetaData;
  setMode();
  loadPDFMetaData( arg );
});

ipc.on('importFinished', function importFinished(event,arg){
  pageMode = modeMain;
  setMode();
})

ipc.on( 'loadSinglePDF', function ( event, arg ) {
  let saveIndex = MetaDataFromDatabase?.index 
  let freshOrReload = MetaDataFromDatabase?.action
  if (freshOrReload == 'update' ) {
    MetaDataFromDatabaseUpdated.PDF = collectEdittedMetaData(jsonPDF());
    MetaDataFromDatabaseUpdated.updated = setFormEditted;
  } else {
    pageMode = modeEditMetaData;
    setMode();
  }
  loadPDF( event, arg, freshOrReload);
  if (freshOrReload) { 
    MetaDataFromDatabase.action = freshOrReload;
    MetaDataFromDatabase.index = saveIndex;
    MetaDataFromDatabase.PDF.$loki = saveIndex
    if (freshOrReload == 'update' ) {
      MetaDataFromDatabase.PDF = JSON.parse(JSON.stringify(MetaDataFromDatabaseUpdated.PDF));
      editPDFSetEditted();
    }
  }
});

ipc.on('resetMassImports', function (event, arg) {
  // Set backup value
  // Clear Tag Fields
  clearForm();
  document.getElementsByClassName("progress-bar-fill")[0].style.width = "1%";
  document.getElementsByClassName("progress-bar-fill")[0].innerHTML = "";
});

ipc.on('clearpdfButtons', function (event, arg) {
  clearButtons();
});

ipc.on("StartLoadingDatabase", function (event, arg) {
 // Stub in "empty" filled table?

});

ipc.on("loaddata", function (event, arg) {
    // Set up the table
    formatGridData(JSON.parse(JSON.stringify(arg)));
});
  
ipc.on("prep", function (event, arg) {
  buttons = headerButtons;
  for ( var bLoop = 0; bLoop < buttons.length; bLoop++)
  { 
    if(document.getElementById(buttons[ bLoop ])) {
      document.getElementById(buttons[ bLoop ]).addEventListener("click", function(e) {
        let src = e.target;
        if( src.children.length === 0 ) { src = src.parentElement; }
        ipc.send(src.id,"");
      } );
    }
  }

  // Copy out and nuke HTML segments.
  // May be better practice to load from single files. 
  let multi = document.getElementById('massImportArea');
  multiInsertBlock = multi.cloneNode(true);
  multi.remove();

  let edit = document.getElementById('editArea');
  editMetadataBlock = edit.cloneNode(true);
  edit.remove();

  let configB = document.getElementById('configureArea');
  configBlock = configB.cloneNode(true);
  configB.remove();

  let metadata = document.getElementById('metaDataDisplayArea');
  metadataPluginBlock = metadata.cloneNode(true);
  metadata.remove();
});

ipc.on("sendEditEntry", function (event, arg) {
  target = document.elementFromPoint(arg.x, arg.y);
  parent = target.parentElement;
  // This should ALWAYS be element zero, but let's be paranoid.
  for (childLoop = 0; childLoop < parent.children.length; childLoop++){
    if ( parent.children[childLoop].getAttribute('tabulator-field') == 'index' ) {
      ipc.send("editTitle", target.parentElement.children[childLoop].innerHTML );    
      break;
    }
  }
});

ipc.on("sendDeleteEntry", function (event, arg) {
  target = document.elementFromPoint(arg.x, arg.y);
  parent = target.parentElement;
  // This should ALWAYS be element zero, but let's be paranoid.
  for (childLoop = 0; childLoop < parent.children.length; childLoop++){
    if ( parent.children[childLoop].getAttribute('tabulator-field') == 'index' ) {
      ipc.send("deleteTitle", target.parentElement.children[childLoop].innerHTML );  
      break;
    }
  }
});

// Mass Import 

ipc.on( 'loadSquentialPDF', function ( event, arg ) {
  if ( pageMode != modeMultiInsert ) {
    pageMode = modeMultiInsert;
    setMode();
  }
  loadPDF( event, arg, 'multiinsert');
});

ipc.on( 'btn_main_editPDF_save', function ( event, arg ) {
  pageMode = modeMain;
  setMode();
});

function jsonPDF () {
  return( {
            productline: {
              gameline:"",
              gamesystem:[],
              required:"",
            },
            production: {
              Publisher:"",
              SequentialTitle:"",
              Title:"",
              Authors: [],
              Editors: [],
              Artists: [],
              PageCount: 0,
              PageFormat: "",
              FileFormat: "",
              PrinterFriendly: false,
              BlackandWhite: false,
              Indexed: false,
              Thumbnails: "",
              Interactive: false
            },
            playstyle: {
              GameType: "",
              Genres: "",
              Subgenres: "",
              GM: "",
              PlayerAgency: "",
              Crunchiness: ""
            },
            filesize: 0,
            md5: "",
            published: "",
            filepath: "",
            originalfilename: "",
            tags: []
          });
}

function collectEdittedMetaData(arg) {
  MetaDataFromDatabaseUpdated.PDF = JSON.parse(JSON.stringify(arg));

  //  = formValidate("", "string", "" );
  //  = formValidate("", "cArray", [] );
  //  = formValidate("", "bool", false );
  //  = formValidate("", "int", 0 );

  // Product Line
  MetaDataFromDatabaseUpdated.PDF.productline.gameline = formValidate( "productline.gameline", "string", "" );
  MetaDataFromDatabaseUpdated.PDF.productline.required = formValidate( "productline.required", "string", "" );

  // Production

  MetaDataFromDatabaseUpdated.PDF.production.Publisher = formValidate("production.publisher", "string", "" );
  MetaDataFromDatabaseUpdated.PDF.production.SequentialTitle = formValidate("production.Sequentialtitle", "string", "" );
  MetaDataFromDatabaseUpdated.PDF.production.Title = formValidate("production.title", "string", "" );
  MetaDataFromDatabaseUpdated.PDF.production.Editors = formValidate("production.editors", "cArray", [] );
  MetaDataFromDatabaseUpdated.PDF.production.Artists = formValidate("production.artists", "cArray", [] );
  MetaDataFromDatabaseUpdated.PDF.production.PageCount = formValidate("production.pagecount", "int", 0 );
  MetaDataFromDatabaseUpdated.PDF.production.PageFormat = formValidate("production.pageformat", "string", "" );
  MetaDataFromDatabaseUpdated.PDF.production.FileFormat = formValidate("production.fileformat", "string", "PDF" );

  // Playstyle

  MetaDataFromDatabaseUpdated.PDF.playstyle.GameType = formValidate("playstyle.gametype", "string", "" );
  MetaDataFromDatabaseUpdated.PDF.playstyle.Genres = formValidate("playstyle.genres", "string", "" );
  MetaDataFromDatabaseUpdated.PDF.playstyle.Subgenres = formValidate("playstyle.subgenres", "string", "" );
  MetaDataFromDatabaseUpdated.PDF.playstyle.GM = formValidate("playstyle.gm", "string", "" );
  MetaDataFromDatabaseUpdated.PDF.playstyle.PlayerAgency = formValidate("playstyle.playeragency", "string", "" );
  MetaDataFromDatabaseUpdated.PDF.playstyle.Crunchiness = formValidate("playstyle.crunchiness", "string", "" );

  // Leftovers
  MetaDataFromDatabaseUpdated.PDF.production.rights = formValidate("production.rights", "string", "" );
  MetaDataFromDatabaseUpdated.PDF.production.url = formValidate("production.url", "string", "" );

  // Pulled from sources
  MetaDataFromDatabaseUpdated.PDF.md5 = MetaDataFromDatabase.PDF.md5;
  MetaDataFromDatabaseUpdated.PDF.filesize = MetaDataFromDatabase.PDF.filesize;
  MetaDataFromDatabaseUpdated.PDF.filepath = MetaDataFromDatabase.PDF.filepath;
  MetaDataFromDatabaseUpdated.PDF.originalfilename = MetaDataFromDatabase.PDF.originalfilename;

  MetaDataFromDatabaseUpdated.PDF.production.Authors = TagClouds['production.authors'].tags;
  MetaDataFromDatabaseUpdated.PDF.production.Artists = TagClouds['production.artists'].tags;
  MetaDataFromDatabaseUpdated.PDF.production.Editors = TagClouds['production.editors'].tags;


  MetaDataFromDatabaseUpdated.PDF.production.Thumbnails = document.getElementById("production.thumbnails").src;

  return MetaDataFromDatabaseUpdated.PDF;
}

ipc.on('sendPDFValues', function (event, arg) {
  let sendBack = collectEdittedMetaData(arg);
  event.sender.send( 'sendPDFValuesBack', { action: MetaDataFromDatabase.action, PDF: sendBack, index: MetaDataFromDatabase.PDF.$loki, previous: MetaDataFromDatabase } );
});

function loadPDF( event, arg, insertType)
{
  let filename = arg.filename;
  console.info("Loading "+ filename);
  clearForm();
  loadedPDFPromise = PDFJS.getDocument( {url: filename,disableAutoFetch: true, disableStream: true} );
  loadedPDFPromise.promise.then( function (pdfDocument) {
    waterfall( [ function(cb) {
      document.getElementById("formTitle").innerHTML = "Loading " + filename;
      pdfDocument.getPage(1).then( function (page) {
        let scale = 1.0;
        canvas = document.createElement('canvas');

        canvasCtx = canvas.getContext('2d');
        viewport = page.getViewport({scale: scale})

        canvas.height = viewport.height
        canvas.width = viewport.width

        var renderer = {
          canvasContext: canvasCtx,
          viewport: viewport
        }

        let renderResults = page.render(renderer);

        renderResults.promise.then(function () {
          canvasCtx.globalCompositeOperation = 'destination-over';
          canvasCtx.fillStyle = '#ffffff';
          canvasCtx.fillRect(0, 0, canvas.width, canvas.height);

          let img = canvas.toDataURL('image/webp');
          document.getElementById('production.thumbnails').setAttribute('src', img);
          canvasCtx = null;
          canvas = null;
          cb(null);
        }, function(reason){
          canvasCtx = null;
          canvas = null;
          console.error(reason);
        })
      }, function (reason) {
        // PDF loading error
        canvasCtx = null;
        canvas = null;
      console.error(reason);
      });
    },
    function (cb) {
      pdfDocument.getMetadata().then( function(metadata) {
        // Fields we care about:
        let basicFields = [ 'Author', 'Title', 'Keywords' ];
        let metaDataArray = [ 'xap:thumbnails', 'dc:format', 'dc:rights', 'dc:title', 'dc:creator', 'xaprights:webstatement'];
        let importData = arg.jsonPDF;

        // Test and set object fields.
        // Basic Data
        if ( metadata.info.hasOwnProperty('Author') )  { importData.production.Authors = splitAppend( importData.production.Authors, metadata.info.Author); }
        if ( metadata.info.hasOwnProperty('Title') ) { importData.production.Title = metadata.info.Title; }
        if ( metadata.info.hasOwnProperty('Keywords') ) { splitAppend( importData.tags, metadata.info.Keywords ); }
        // Extended Metadata
        if ( goodProperty( metadata,'metadata' ) ) {
          if ( goodProperty( metadata.metadata, '_metadata' ) ) {
            let m = metadata.metadata._metadata;
            if ("xap:thumbnails" in m) { importData.production.thumbnail = m[ "xap:thumbnails" ];}
            if ("dc:rights" in m) { importData.rights = m[ "dc:rights" ];}
            if ("dc:title" in m) { if (!importData.production.Title) importData.production.Title = m[ "dc:title" ];}
            if ("dc:creator" in m) { if (importData.production.Authors.length < 1 ) { importData.production.Author = splitAppend( importData.production.Author, m[ "dc:creator" ]); } }
            if ("xaprights:webstatement" in m) { importData.url = m[ "xaprights:webstatement" ];}
          }
        }

        const stats = fs.statSync(filename);
        importData.filesize = stats.size;
        importData.production.FileFormat = "PDF";
        importData.production.PageCount = pdfDocument.numPages;    
        importData.filepath = filename;
        importData.filename = path.basename(filename);
        importData.production.Thumbnails = document.getElementById('production.thumbnails').getAttribute('src');

        pdfDocument.cleanup();
        pdfDocument.destroy();

        // MUST BE LAST!
        var hash = crypto.createHash('md5'),
        stream = fs.createReadStream(filename); //, {highWaterMark: 8*1024}
        stream.on('data', function (data) {
          hash.update(data, 'utf8')
        })


        stream.on('close', function () {
          importData.md5 = hash.digest('hex');
          
          if (MetaDataFromDatabase?.index > 0) {
            loadPDFMetaData( { action: insertType, PDF: importData, index: MetaDataFromDatabase?.index } );
            editPDFSetEditted();
            MetaDataFromDatabase.PDF = JSON.parse(JSON.stringify(MetaDataFromDatabaseUpdated.PDF));
            MetaDataFromDatabase.PDF.$loki = MetaDataFromDatabase?.index;
          } else {
            loadPDFMetaData( { action: insertType, PDF: importData, position: arg.position } );
          }
          cb(null);
        })
      });
    }],);
  }).catch((err)=>{
    this.showError = true;
    console.error(err);
    failedImport(arg.filename)
  })
}


function setForm( PDFData, percentage ) {

  function setFormField( fieldName, testValue, joinOrCopy, defaultValue, initial )
  {
    let defaultV;
    let field;
    field = document.getElementById(fieldName);
    let lastValue = field.value;
    defaultV = ( field.value ? field.value : defaultValue );
    let setTo = goodCopy( testValue, joinOrCopy, defaultV );
    if( field.nodeName == "INPUT") { field.value = setTo; }
    else { field.innerHTML = setTo; }
    if ( ( ( PDFData !== MetaDataFromDatabase.PDF ) && ( PDFData !== MetaDataFromDatabaseUpdated.PDF ) ) && ( setTo !== lastValue ) )
    {
      field.parentElement.children[0].className = "metaDataChangedLabel";
      if ( pageMode == modeEditMetaData ) editPDFSetEditted();
    }
  }

  var arrayCompare = function (value, other) {

	// Get the value type
	var type = Object.prototype.toString.call(value);

	// If the two objects are not the same type, return false
	if (type !== Object.prototype.toString.call(other)) return false;

	// If items are not an object or array, return false
	if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

	// Compare the length of the length of the two items
	var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
	var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
	if (valueLen !== otherLen) return false;

	// Compare two items
	var arrayCompare = function (item1, item2) {
  		// Get the object type
  		var itemType = Object.prototype.toString.call(item1);

  		// If an object or array, compare recursively
  		if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
  			if (!isEqual(item1, item2)) return false;
  		}

  		// Otherwise, do a simple comparison
  		else {

  			// If the two items are not the same type, return false
  			if (itemType !== Object.prototype.toString.call(item2)) return false;

  			// Else if it's a function, convert to a string and compare
  			// Otherwise, just compare
  			if (itemType === '[object Function]') {
  				if (item1.toString() !== item2.toString()) return false;
  			} else {
  				if (item1 !== item2) return false;
  			}

  		}
  	};

  	// Compare properties
  	if (type === '[object Array]') {
  		for (var i = 0; i < valueLen; i++) {
  			if (arrayCompare(value[i], other[i]) === false) return false;
  		}
  	} else {
  		for (var key in value) {
  			if (value.hasOwnProperty(key)) {
  				if (arrayCompare(value[key], other[key]) === false) return false;
  			}
  		}
  	}

  	// If nothing failed, return true
  	return true;

  };

  if( ( PDFData !== MetaDataFromDatabase.PDF ) && (PDFData !== MetaDataFromDatabaseUpdated.PDF ) ) {
    if ( !arrayCompare( PDFData.tags, MetaDataFromDatabase.PDF.tags ) ) { document.getElementById('tags').className = "metaDataTagsChangedList"; editPDFSetEditted(); }
    if ( !arrayCompare( PDFData.production.Authors, MetaDataFromDatabase.PDF.production.Authors ) ) { document.getElementById('production.authors').parentElement.children[0].className = "metaDataChangedLabel"; editPDFSetEditted(); }
    if ( !arrayCompare( PDFData.production.Artists, MetaDataFromDatabase.PDF.production.Artists ) ) { document.getElementById('production.artists').parentElement.children[0].className = "metaDataChangedLabel"; editPDFSetEditted(); }
    if ( !arrayCompare( PDFData.production.Editors, MetaDataFromDatabase.PDF.production.Editors ) ) { document.getElementById('production.editors').parentElement.children[0].className = "metaDataChangedLabel"; editPDFSetEditted(); }
    if ( !arrayCompare( PDFData.productline.gamesystem, MetaDataFromDatabase.PDF.productline.gamesystem ) ) { document.getElementById('productline.gamesystem').parentElement.children[0].className = "metaDataChangedLabel"; editPDFSetEditted(); }
  }
  
  TagClouds['production.authors'] = new TagsCloudInput('production.authors');
  TagClouds['production.artists'] = new TagsCloudInput('production.artists');
  TagClouds['production.editors'] = new TagsCloudInput('production.editors');

  // Change the title
  document.getElementById("formTitle").innerHTML = "Edit Metadata for " + goodCopy( PDFData.production.Title, false, path.basename(PDFData.filepath));
  if ( document.getElementsByClassName("progress-bar-fill")[0]?.style ) {
    if (percentage > 1 ) {
      document.getElementsByClassName("progress-bar-fill")[0].style.width = percentage + "%";
      if (percentage > 3 ) {
        document.getElementsByClassName("progress-bar-fill")[0].innerHTML = percentage + "%&nbsp;&nbsp;";
      }
    }
  }

  // Populate form fields

  // Product Line

  setFormField( "productline.gameline", PDFData.productline.gameline, false, '');
  setFormField( "productline.required", PDFData.productline.required, false, '');

  // Production

  setFormField( "production.publisher", PDFData.production.Publisher, false, '');
  setFormField( "production.sequentialtitle", PDFData.production.SequentialTitle, false, '');
  setFormField( "production.title", PDFData.production.Title, false, '');
  setFormField( "production.editors", PDFData.production.Editors, true, []);
  setFormField( "production.artists", PDFData.production.Artists, true, []);
  setFormField( "production.pagecount", PDFData.production.PageCount, false, '');
  setFormField( "production.pageformat", PDFData.production.PageFormat, false, '');
  setFormField( "production.fileformat", PDFData.production.FileFormat, false, '');

  document.getElementById("production.thumbnails").src = goodCopy( PDFData.production.Thumbnails, false, '');

  // Playstyle

  setFormField( "playstyle.gametype", PDFData.playstyle.GameType, false, '');
  setFormField( "playstyle.genres", PDFData.playstyle.Genres, false, '');
  setFormField( "playstyle.subgenres", PDFData.playstyle.Subgenres, false, '');
  setFormField( "playstyle.gm", PDFData.playstyle.GM, false, '');
  setFormField( "playstyle.playeragency", PDFData.playstyle.PlayerAgency, false, '');
  setFormField( "playstyle.crunchiness", PDFData.playstyle.Crunchiness, false, '');

  // Leftovers

  setFormField( "production.rights", PDFData.production.rights, false, '');
  setFormField( "production.url", PDFData.production.url, false, '');

  // Don't override instance values.
  if( PDFData === MetaDataFromDatabase.PDF ) {
    setFormField( "md5", PDFData.md5, false, '');
    setFormField( "filesize", PDFData.filesize, false, '');
    document.getElementById("filename").value = goodCopy( PDFData.filepath, false, '');
    setFormField( "filepath", PDFData.filepath, false, '');
    setFormField( "filename", path.basename(PDFData.filepath), false, '');
  }

  if ( pageMode == modeMultiInsert ) {
    if ( PDFData.production.Title.length < 1 ) {
      document.getElementById('production.title').parentNode.className = 'hidden';
    }

    if ( (PDFData.production.Authors) && ( PDFData.production.Authors.length > 0 ) ) {
      PDFData.production.Authors.forEach( function( author ) { if ( author ) TagClouds['production.authors'].add( author ); } );
    } else { 
      document.getElementById('production.authors').parentNode.className = 'hidden';
    }
    if ( (PDFData.production.Artists) && ( PDFData.production.Artists.length > 0 ) ) {
      PDFData.production.Artists.forEach( function( artist ) { if ( artist ) TagClouds['production.artists'].add( artist ); } );
    } else { 
      document.getElementById('production.artists').parentNode.className = 'hidden';
    }
    if ( (PDFData.production.Editors) && ( PDFData.production.Editors.length > 0 ) ){
      PDFData.production.Editors.forEach( function( editor ) { if ( editor ) TagClouds['production.editors'].add( editor ); } );
    } else { 
      document.getElementById('production.editors').parentNode.className = 'hidden';
    }

    ipc.send('btn_massImportPDF_save')
  }
}

function loadPDFMetaData( PDFData )
{ 
  MetaDataFromDatabase = JSON.parse(JSON.stringify(PDFData));
  setForm( MetaDataFromDatabase.PDF, MetaDataFromDatabase?.position || MetaDataFromDatabase?.index );
}

function goodCopy( testValue, joinOrCopy, defaultValue )
{
  if( testValue ) {
    if( joinOrCopy )
    {
      return( testValue.join(','))
    }
    else
    {
      return( testValue );
    }
  }
  else {
    return ( defaultValue );
  }
}

function clearButtons() {
  // Clear the button statuses
  for ( var bLoop = 0; bLoop < headerButtons.length; bLoop++)
  {
    if ( document.getElementById(headerButtons[ bLoop ]) ) {
      document.getElementById(headerButtons[ bLoop ]).className = document.getElementById(headerButtons[ bLoop ]).className.replace("btn-warning", "btn-default");
    }
  }
}

function formValidate( formID, shouldBe, defaultValue )
{
  let field = document.getElementById(formID);
  let exVal;
  let fV;

  if ( ( ! field ) || ( ( field instanceof HTMLInputElement) && ( field.value == undefined ) ) ) {
    console.log("EMPTY!")
    return( defaultValue );
  }

  if ( field instanceof HTMLInputElement ) { 
    console.log("INPUT");
    exVal = field.value; 
  } else { 
    exVal = field.innerHTML; 
  }

  if (formID == "production.title" ) { console.log( "Looking at: " + exVal); }

  if ( shouldBe == 'cArray' ) {
    fV = exVal.split(',');
  } else {
    fV = exVal;
  }

  return( fV );
}

function setInnerHtmlIfExists( element, value ) {
  el = document.getElementById( element );
  if (el) {
    el.innerHTML = value;
  }
}

function clearThumbnail(elementID) {
  if( document.getElementById(elementID) ) {
    document.getElementById(elementID).setAttribute('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAAE0lEQVR42mNk+M+AFzCOKhhJCgBrLxABLz0PwwAAAABJRU5ErkJggg==');
  }
}

function clearForm() {

  // Bulk Clear Inputs
  let inputs = document.getElementsByTagName('input');
  for ( var inputLoop=0; inputLoop < inputs.length; inputLoop++)
  {
    inputs[ inputLoop ].value = '';
  }

  let labels = document.getElementsByTagName('label');
  for ( var loop=0; loop < labels.length; loop++)
  {
    if(( labels[loop].className == "metaDataChangedLabel" ) || ( labels[loop].className == "metaDataLabel" )){ 
      labels[loop].className = "metaDataLabel"; 
    } else { 
      labels[loop].className = "metaDataCheckboxLabel" 
    }
  }

  setInnerHtmlIfExists('filename','&nbsp;');
  setInnerHtmlIfExists('filesize','&nbsp;');
  setInnerHtmlIfExists('md5','&nbsp;');
  setInnerHtmlIfExists('production.pagecount','&nbsp;');
  clearThumbnail('production.thumbnails');

  // Clear Tag Fields
  for ( var cloud in TagClouds )
  {
    TagClouds[ cloud ].tags.forEach( function( tag ) {
      TagClouds[ cloud ].remove( tag );
    } )
  }

  document.getElementById('production.authors').parentNode.className = 'form-group';
  document.getElementById('production.artists').parentNode.className = 'form-group';
  document.getElementById('production.editors').parentNode.className = 'form-group';
  document.getElementById('production.title').parentNode.className = 'form-group';

  //clearButtons();
}

function failedImport(filename) {
  ipc.send('btn_importPDF_failed')
}

function resizeTitlesTable() {
  if (tableRowHeight == -1 ) {
    tableRows = document.getElementsByClassName('tabulator-row tabulator-selectable tabulator-row-odd');
    box = tableRows[0];
    if (box) {
      style = getComputedStyle(box);
      marginTop = parseInt(style.marginTop);
      marginBottom = parseInt(style.marginBottom);
      borderTopWidth = parseInt(style.borderTopWidth) || 0;
      borderBottomWidth = parseInt(style.borderBottomWidth) || 0;

      tableRowHeight = document.getElementsByClassName('tabulator-row tabulator-selectable tabulator-row-odd')[0].offsetHeight + marginTop + marginBottom + borderTopWidth + borderBottomWidth;
    }
  }

  let height = document.documentElement.clientHeight;
  let width = document.documentElement.clientWidth;
  
  if ( ( (height - tableRowHeight) != lastWindowSizeHeight) || (width != lastWindowSizeWidth) ) {
    lastWindowSizeHeight = height;
    lastWindowSizeWidth = width;
    tabulatorObject = null;
    formatGridData( [], true);
  }
}

var resizeTitlesTableTimerHandle;
window.addEventListener('resize', function setResizeDetectedTimer(){
  clearTimeout(resizeTitlesTableTimerHandle);
  resizeTitlesTableTimerHandle = this.setTimeout(resizeTitlesTable, 100);
});

