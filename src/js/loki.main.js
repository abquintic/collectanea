"use strict";
const loki = require('lokijs');
const lokiFSStructuredAdapter = require('lokijs/src/loki-fs-structured-adapter.js');

function Deferred() {
  var self = this;
  this.promise = new Promise(function(resolve, reject) {
    self.reject = reject
    self.resolve = resolve
  })
}

module.exports.constructor = function ( dbName ) {
    var fsa = new lokiFSStructuredAdapter('appAdapter');
      exports.db = new loki(dbName, {
      	autoload: true,
      	autoloadCallback: databaseInitialize,
      	autosave: true,
      	autosaveInterval: 300,
        adapter: fsa,
        throttledSaves: true,
        verbose:true,
        env: 'NODEJS',
    });
    exports.loaded = new Deferred();
  };

 function addDefaultTags() {
    // Tag Structure
    // tag: string - simple name
    // desc: short description of the tag
    // long: long description of the tag

    let tagCol = exports.db.getCollection('tags');
    tagCol.insert({tag:'Fantasy', desc:'',long:''});
    tagCol.insert({tag:'SpaceOpera', desc:'',long:''});
    tagCol.insert({tag:'HardScience', desc:'',long:''});
    tagCol.insert({tag:'Horror', desc:'',long:''});
  }


  // Make sure the database exists etc.
function databaseInitialize() {
    var collections = [ 'books', 'covers', 'tags' ];
    collections.forEach( function( colName ){
      let col = exports.db.getCollection(colName);
      if (col === null) {
        col = exports.db.addCollection(colName);
        var entryCount = exports.db.getCollection(colName).count();
      }
    });

    addDefaultTags();
    console.info("Database Initialiization Completed.");
    exports.loaded.resolve('Loaded.');
  }
