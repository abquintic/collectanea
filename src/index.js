require("v8").setFlagsFromString("--expose_gc");
require('v8-compile-cache');

global.gc = require("vm").runInNewContext("gc");

const { app, dialog, BrowserWindow, ipcMain, Menu } = require('electron');

const loki = require('./js/loki.main.js');

const os = require('os');
const settings = require('electron-settings');
const crypto = require('crypto')
const fs = require('fs');
const util = require('util');
const waterfall = require('async-waterfall');
const path = require('path');
const plugins = require('@abquintic/electron-plugins');
const AppDirectory = require("appdirectory");
const shell = require('electron').shell;

const PDFWindow = require('electron-pdf-window')


var lokiCollections = {
  books: null,
  covers: null,
  tags: null,
}

const preloadFiles = [
    path.join(__dirname, "js/index.include.js"),
  ];

let windows = [];
let massParse = [];
let currentPDF = {};

// Utility Functions

function createTimeoutPromise ( timeOut, passedValue, actions ) {
  // The util.promisify routines in the example docs are not right. Endrunning for now.
  const setTimeoutPromise = setTimeout( actions, timeOut, passedValue );
}

// from https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+_:";

  for (var i = 0; i < 15; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

function updateSplashScreen(message){
  if (windows['splashWindow'] != null ) {
    windows['splashWindow'].webContents.send('updateLoading', message);
  }
}

function jsonPDF () {
  return( {
            productline: {
              gameline:"",
              gamesystem:[],
              required:"",
            },
            production: {
              Publisher:"",
              SequentialTitle:"",
              Title:"",
              Authors: [],
              Editors: [],
              Artists: [],
              PageCount: 0,
              PageFormat: "",
              FileFormat: "",
              PrinterFriendly: false,
              BlackandWhite: false,
              Indexed: false,
              Thumbnails: "",
              Interactive: false
            },
            playstyle: {
              GameType: "",
              Genres: "",
              Subgenres: "",
              GM: "",
              PlayerAgency: "",
              Crunchiness: ""
            },
            filesize: 0,
            md5: "",
            published: "",
            filepath: "",
            originalfilename: "",
            tags: []
          });
}

function parseDirectory( folder ) {
  massparse = [];
  let filePath = folder[ 0 ];
  readFiles = getAllFiles( filePath );
  readFiles.forEach(function(name) {
    if ( path.extname( name ).toUpperCase() == ".PDF" ) {
      massParse.push( name )
    }
  });
  if ( massParse.length > 0 ) {
    massParseCount = massParse.length;
    mainWindow.webContents.send('resetMassImports', null);
    mainWindow.webContents.send('loadSquentialPDF', { filename: massParse[ 0 ], position: 0, jsonPDF: jsonPDF() } );
  }
}

function parsePDF( filenames ) {
  // brute force single input for now.
  if( filenames ) {
    let filename = filenames[0];
    mainWindow.webContents.send('loadSinglePDF', { filename: filename, jsonPDF: jsonPDF() } );
  }
}

function getAllFiles(dirPath, arrayOfFiles) {
  files = fs.readdirSync(dirPath)

  arrayOfFiles = arrayOfFiles || []

  files.forEach(function(file) {
    if (fs.statSync(dirPath + "/" + file).isDirectory()) {
      arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
    } else {
      arrayOfFiles.push(path.join(dirPath, "/", file))
    }
  });
  return arrayOfFiles

}

function removeFromCollectaneaContents( arg ) {
  let whichTitleIndex = parseInt( arg, 10 );
  try {
    if ( lokiCollections.books && lokiCollections.covers) {
      let bookLookup = lokiCollections.books.findOne({ '$loki': whichTitleIndex });
      if ( bookLookup ) {
        let whichCoverIndex = parseInt( bookLookup.production.Thumbnails, 10);
        let coverLookup = lokiCollections.covers.findOne({ '$loki': whichCoverIndex });
        lokiCollections.books.remove(bookLookup);
        if ( coverLookup ) lokiCollections.covers.remove(coverLookup);
      }
    } else {
      return null;
    }
  } catch (error) {
    console.error(error);
    return null
  }
}

function loadCollectaneaContents()
{
  // Attempt to load the collection from the database.
  try {
    if ( lokiCollections.books )
    {
      return (lokiCollections.books.find({}));
    } else {
      return null;
    }
  } catch (error) {
    console.error(error);
    return null
  }
}


// Initialization Functions

function initializeSettings()
{
  // Brute force for now.
  let settingsObject = {
    version: 0
  }

  settings.setSync('global', settingsObject );
  settings.setSync('taggedstorage', { base: '.local/share/Collectanea/storage', organization: '.local/share/Collectanea/Library'});
}

function setupIPCListeners() {

  // Configuration Panel
  ipcMain.on('btn_main_config_close', function (event, arg) {
    event.sender.send('clearButtons', '');
    windows['configWindow'].hide();
  });
  ipcMain.on('btn_main_config_thumbs_up', function (event, arg) {
    // Save the changes!
    event.sender.send('sendConfigurationValues', 'PLEASE');
  });
  ipcMain.on('btn_main_config_thumbs_down', function (event, arg) {
    // Revert the changes!
    event.sender.send('loadConfigurationValues', backupSettings );
    event.sender.send('clearButtons', '');
  });
  ipcMain.on('btn_main_config_debug', function (event, arg) {
    windows['configWindow'].webContents.toggleDevTools();
  });
  ipcMain.on('sendConfigurationValuesBack', function (event, arg) {
    settings.setAll(arg);
    event.sender.send('clearButtons', '');
  });
  ipcMain.on('deleteTitle', function deletetitleAction(event,arg){
    removeFromCollectaneaContents(arg);
    mainWindow.webContents.send('loaddata', loadCollectaneaContents()); 
  });

  // PDF edit Buttons
  
  ipcMain.on('btn_main_editPDF_save',  function (event, arg) {
    event.sender.send('sendPDFValues', jsonPDF());
    mainWindow.webContents.send('clearpdfButtons', '' );
  });

  ipcMain.on('btn_main_massImportPDF_save',  function (event, arg) {
    event.sender.send('sendPDFValues', jsonPDF());
    mainWindow.webContents.send('clearpdfButtons', '' );
  });

  ipcMain.on('btn_main_importPDF_failed',  function (event, arg) {
    event.sender.send('resetForm', currentPDF );
    mainWindow.webContents.send('clearpdfButtons', '' );
    openNextPDF();
  });

  ipcMain.on('btn_main_editPDF_reset',  function (event, arg) {
    event.sender.send('resetForm', currentPDF );
  });

  ipcMain.on('btn_main_editPDF_cancel', function (event, arg) {
    mainWindow.webContents.send('importFinished', {});
    titles=loadCollectaneaContents();
    mainWindow.webContents.send('loaddata', titles);
  });
  ipcMain.on('sendPDFValuesBack', function (event, arg) {
    savePDF( arg );
    event.sender.send('clearpdfButtons', '' );
  });
  ipcMain.on('getPDFData',  function (event, arg) {
    event.sender.send('loadForm', { action: 'update', PDF: currentPDF } );
    event.sender.send('clearpdfButtons', '' );
  });
  ipcMain.on('btn_main_editPDF_debug',  function (event, arg) {
    // mainWindow.webContents.openDevTools();
  });

  // Main Panel Buttons
  ipcMain.on('btn_main_config', function (event, arg) {
    openConfiguration();
  });
  ipcMain.on('btn_main_plugins', function (event, arg) {
  });
  ipcMain.on('btn_main_import', function (event, arg) {
    dialog.showOpenDialog({ filters: [ { name: 'E-Books', extensions: ['pdf'] } ], properties: [ 'openFile' ] }).then((data) => {
      if ( ! data.cancel ) {
        parsePDF(data.filePaths);
      }
    });
  });
  ipcMain.on('btn_main_massimport', function (event, arg) {
    dialog.showOpenDialog({ properties: [ 'openDirectory' ]}).then((data) => {
      if ( ! data.canceled ) {
        parseDirectory(data.filePaths);
      }
    });
  });
  ipcMain.on('btn_massImportPDF_save',  function (event, arg) {
    event.sender.send('sendPDFValues', jsonPDF());
    event.sender.send('clearpdfButtons', '' );
  });
  ipcMain.on('btn_importPDF_failed',  function (event, arg) {
    event.sender.send('resetForm', currentPDF );
    event.sender.send('clearpdfButtons', '' );
    openNextPDF();
  });


  ipcMain.on('btn_main_export', function (event, arg) {
  });
  ipcMain.on('btn_main_collection', function (event, arg) {
  });
  ipcMain.on('btn_main_search', function (event, arg) {
  });
  ipcMain.on('btn_main_close', function (event, arg) {
    mainWindow.close();
  });
  ipcMain.on('btn_main_maximize', function (event, arg) {
  });
  ipcMain.on('btn_main_minimize', function (event, arg) {
  });
  ipcMain.on('btn_main_tray', function (event, arg) {
  });

  ipcMain.on('btn_main_editPDF_reloadMetaFromFile', function(event,arg) {
    let cleanPDF = jsonPDF();
    cleanPDF.$loki = arg.$loki;
    mainWindow.webContents.send('loadSinglePDF', { filename: arg.filepath, jsonPDF: cleanPDF, index: arg.$loki, reload: true } );
  });

  ipcMain.on('launchTitle', function (event, arg ) {
    let whichTitleIndex = parseInt( arg, 10 );

    var results = lokiCollections.books.findOne( { '$loki': whichTitleIndex } );

    filepath = encodeURIComponent(path.normalize(path.join(__dirname, path.relative(__dirname,results.filepath))));

    const win = new PDFWindow({
      width: 800,
      height: 600
    })

    win.loadURL(encodeURIComponent(filepath));

  });

  ipcMain.on('metadataSelected', function(event, arg) {
    mainWindow.webContents.send("metadataResults", arg);
    windows['selectMetadataWindow'].hide();
  });

  ipcMain.on('metadatasearch', function (event, arg) {
    mainWindow.webContents.send("resetMetaDataResults", {} );
    let pluginCount = 0;
    mainWindow.webContents.send('RunPlugins', arg);

    for( var pLoop=0; pLoop < loadedPlugins.length; pLoop++)
    {
      if ( arg.plugins.includes( loadedPlugins[ pLoop ].id ) ) {
        loadedPlugins[ pLoop ].metadata( require("axios"), require("cheerio"), mainWindow.webContents, arg );
        pluginCount = pluginCount + 1;
      }
    }
    mainWindow.webContents.send('PluginCount', pluginCount);
  });

  ipcMain.on('editTitle', function (event, arg ) {
    let whichTitleIndex = parseInt( arg, 10 );
    var results = lokiCollections.books.findOne( { '$loki': whichTitleIndex } );
    let editResults = JSON.parse(JSON.stringify(results));
    if ( results.production.Thumbnails ) {
      whichCover = parseInt(results.production.Thumbnails, 10);
      var cover = lokiCollections.covers.findOne( { '$loki': results.production.Thumbnails});
      if (cover) {
        editResults.production.Thumbnails = cover.cover
      }
    }
    mainWindow.webContents.send('editPDFData', { action: 'update', PDF: editResults, index: whichTitleIndex } );
  });

  ipcMain.on("loadedPlugin", function (event, arg) {
    // Bounce this to the appropriate window by type.
    arg.id = makeid();
    if( arg.pluginType.includes("metadata") ) {
      mainWindow.webContents.send('loadedPlugin', arg);
    }
  })

}

function duplicatePDFCheck(PDFJSON, collection) {
  duplicates = collection.find({ 'md5': PDFJSON.md5, 'filesize': PDFJSON.filesize})
  return( duplicates.length == 0 )
}

function openNextPDF() {
  massParse.splice(0,1);
  if ( massParse.length > 0 ) {
    mainWindow.webContents.send('loadSquentialPDF', { filename: massParse[ 0 ], jsonPDF: jsonPDF(), position: Math.floor((massParseCount-massParse.length)*100/massParseCount) } );
  } else { 
    mainWindow.webContents.send('importFinished', {});
    titles=loadCollectaneaContents();
    mainWindow.webContents.send('loaddata', titles);
  }
}


function savePDF( importData ) {

  // Move the thumbnail into it's own variable and clean it out of the importData

  var importBook = {}
  var importCover = {
    book: "",
    cover: importData.PDF.production.Thumbnails
  }

  importBook = JSON.parse(JSON.stringify(importData.PDF));
  // This will get reassigned to a $loki reference for the covers collection.
  importBook.production.Thumbnails = "";  


  function insertPDF() {
      // Insert the cover into the collection.
      coverResultObj = lokiCollections.covers.insert( importCover );

      // Assign the cover reference to the thumbnails value.
      importBook.production.Thumbnails = coverResultObj.$loki;
      // Insert the cover into the collection.
      bookResultObj = lokiCollections.books.insert( importBook );

      importCover.book = bookResultObj.$loki;

      // Update the Cover with the title's $loki reference.
      lokiCollections.covers.update( importCover );
      return bookResultObj.$loki;
  }

  function updatePDF() {
    // Get the stored objects.
    let bookResult = lokiCollections.books.findOne( { '$loki': importData.index } );
    let coverResult = lokiCollections.covers.findOne( {'$loki': bookResult.production.Thumbnails});

    // Duplicate the updated values.
    
    coverResult.cover = importData.PDF.production.Thumbnails;

    bookResultObj = Object.assign(bookResult, importData.PDF );
    bookResultObj.production.Thumbnails = coverResult.$loki;


    // Update the stored values.
    lokiCollections.books.update( bookResultObj );
    lokiCollections.covers.update( coverResult );
  }

  for ( var pLoop=0; pLoop < loadedPlugins.length; pLoop++ )
  {
    if( loadedPlugins[ pLoop ].pluginType.slice(0) == 'storage' )
    {
      importData = loadedPlugins[ pLoop ].store( settings.getSync('taggedstorage'), false, importData )
    }
  }

  if( importData.action == 'insert' ) {
    if (duplicatePDFCheck(importData.PDF, lokiCollections.books))
    {
      newIndex = insertPDF();
      mainWindow.webContents.send('loadForm', { action: 'update', PDF: importData.PDF, index: newIndex } );
      mainWindow.webContents.send('clearpdfButtons', '' );
    }
  } else if ( importData.action == 'multiinsert' ) {
    if (duplicatePDFCheck(importData.PDF, lokiCollections.books))
    {
      newIndex = insertPDF();
    }
    openNextPDF();
  } else {
    updatePDF();
  }
}


function showSplash( ) {
  // This method will be called when Electron has done everything
  // initialization and ready for creating browser windows.
  app.on('ready', function() {
    // Create the browser window.
    windows['splashWindow'] = new BrowserWindow({width: 430, height: 225, frame: false, show: false, 
      disableBlinkFeatures: "Auxclick",
      icon: "./src/icons/icon.png",
      webPreferences: {
        nodeIntegration: false, // is default value after Electron v5
        contextIsolation: true, // protect against prototype pollution
        enableRemoteModule: false, // turn off remote
        //sandbox: true,
        preload: path.join(__dirname, "js/splash.include.js") // use a preload script
    }});

    // and load the index.html of the app.
    windows['splashWindow'].loadURL('file://' + __dirname + '/splashscreen.html');

    windows['splashWindow'].webContents.session.setPermissionCheckHandler((webContents, permission) => {
      return false
    });
    windows['splashWindow'].show();

    windows['splashWindow'].on('closed', function () {
        windows['splashWindow'] = null;
    });

    // windows['splashWindow'].webContents.openDevTools();

  });
}

function setupMainWindow() {

  const {application_menu}  = require(__dirname + '/js/menus.main.js');
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 800, height: 600, show: false, disableBlinkFeatures: "Auxclick", 
  icon: "./src/icons/icon.png",
  webPreferences: {
      nodeIntegration: true, // is default value after Electron v5
      devTools: true,
      contextIsolation: true, // protect against prototype pollution
      enableRemoteModule: false, // turn off remote
      preload: path.join(__dirname, "js/index.include.js"), // use a preload script
      //sandbox: true, // Can't do this until I webpack or the like the preload and depends...
  }});

  // and load the index.html of the app.
  mainWindow.loadURL('file://' + __dirname + '/index.html');
  mainWindow.webContents.openDevTools();

  mainWindow.webContents.session.setPermissionCheckHandler((webContents, permission) => {
    return false
  });

  if( ! settings.getSync('global') )
  {
    initializeSettings();
  }
  else {
    console.info( 'Loading config from ' + settings.file() );
    backupSettings = settings.getSync();
  }

  let menu;

  menu = Menu.buildFromTemplate(application_menu);
  Menu.setApplicationMenu(menu);


  mainWindow.on('ready-to-show', () => {
    mainWindow.webContents.send('prep', 'NOW');
  })


  mainWindow.on('close', function () {
    isQuitting = true;
  });

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    // Stall one second first. Should make sure the DB flushes.
    createTimeoutPromise( 1000, '', function () {  mainWindow = null; } );
  });
}



function setupApp() {

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    if (process.platform != 'darwin')
      app.quit();
  });
  isQuitting = false;

  // Waterfall through the timeouts.

  waterfall( [
    function(cb) {
      createTimeoutPromise(2050, 'Setting up Windows.', function(value) {
        updateSplashScreen(value);
        setupMainWindow();
        cb(null);
      });
    },
    function(cb) {
      createTimeoutPromise(2050, 'Setting up Database.', function(value) {
        var appDir = path.dirname(__dirname);
        updateSplashScreen(value);
        mainWindow.webContents.send('StartLoadingDatabase', null);
        loki.constructor(app.getPath('userData')+'/defaultCollection.json');
        console.info("Database stored in: " +app.getPath('userData')+'/defaultCollection.json')
        cb(null);
      });
    },
    function(cb) {
      createTimeoutPromise(250, 'Setting up Comms.', function(value) {
        updateSplashScreen(value);
        setupIPCListeners();
        cb(null);
      });
    },
    function(cb) {
      var appDir = path.dirname(__dirname);
      var pluginPath = app.getPath('userData')+'/Plugins/'
      plugins.discover( pluginPath, appDir, true, function ( err, results ) {
        var context = { appDir: appDir, pluginPath: pluginPath, makePluginPath: true };
        if( !err )
        {
          context.plugins = results;
          context.quiet = false
        }
        else {
          console.error(err);
        }
        plugins.load({context: context}, function (err, dependencies, modules) {
          if(err) return console.error(err);
          loadedPlugins = modules;
          for ( var pLoop=0; pLoop < loadedPlugins.length; pLoop++ )
          {
            let arg = {};
            loadedPlugins[ pLoop ].id = makeid();
            arg.id = loadedPlugins[ pLoop ].id;
            arg.name = loadedPlugins[ pLoop ].name;
            arg.shortname = loadedPlugins[ pLoop ].shortname;
            arg.version = loadedPlugins[ pLoop ].version;
            arg.description = loadedPlugins[ pLoop ].description;
            arg.pluginType = loadedPlugins[ pLoop ].pluginType.slice(0);

            // currently valid plugin types - metadata, submission, setTimeoutPromise
            if( arg.pluginType.includes("metadata") ) {
              mainWindow.webContents.send('loadedPlugin', arg);
            }
          }
        });
      });
      cb(null);
    },
    function(cb) {
      loki.loaded.promise.then(function WaitingForDBLoad() {
      updateSplashScreen('Loading Book Library.' );

      // Grab the global loki collections.

      lokiCollections.books = loki.db.getCollection('books');
      if ( !lokiCollections.books ) {
        loki.db.addCollection('books');
        lokiCollections.books = loki.db.getCollection('books');
      }
  
      lokiCollections.covers = loki.db.getCollection('covers');
      if ( !loki.db.getCollection('covers') ) {
        loki.db.addCollection('covers');
        lokiCollections.covers = loki.db.getCollection('covers');
      }

      lokiCollections.tags = loki.db.getCollection('tags');
      if ( !loki.db.getCollection('tags') ) {
        loki.db.addCollection('tags');
        lokiCollections.tags = loki.db.getCollection('tags');
      }

      loki.loaded.promise.then( function LoadInitialDB () {
        bookCollection = loadCollectaneaContents();
        mainWindow.webContents.send('loaddata', bookCollection);
        mainWindow.show();
        windows['splashWindow'].close();
      });
    });
    cb(null);
    }
  ], null);
}


function applicationMain() {
  app.commandLine.appendSwitch('js-flags','--expose_gc --max-old-space-size=2048');
  app.disableHardwareAcceleration();
  showSplash();

  ipcMain.on('loadApp', function (event, arg) { setupApp(); } );
}

applicationMain();
